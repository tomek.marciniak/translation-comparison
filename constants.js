const BASELINE = "https://feature-pia-189175-baseline-app-shell.picsartstage2.com";
const CHANGED = "https://feature-pia-189175-translations-removed-app-shell.picsartstage2.com";
const SLUGS = "https://api-landings.picsart.com/api/page-data/slugs";

module.exports = {
  BASELINE,
  CHANGED,
  SLUGS,
}