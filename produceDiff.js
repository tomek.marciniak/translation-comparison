const Diff = require('diff');
require('colors');

const produceDiff = (baseline, changed) => {
  const diff = Diff.diffChars(baseline, changed);
  const addedParts = [];

  diff.forEach((part) => {
    if (part.added) {
      addedParts.push(part.value);
    }
  });
  return addedParts.length ? addedParts.join(',') : '-';
}

module.exports = produceDiff;