const checkPage = require('./checkPage');
const fetchPageSlugs = require('./fetchPageSlugs');
const saveResult = require('./saveResult');

(async () => {
  const pageSlugs = await fetchPageSlugs()

  for (const slug of pageSlugs) {
    const diff = await checkPage(slug);
    await saveResult({ slug, diff });
  }

  await saveResult(results);
})();
