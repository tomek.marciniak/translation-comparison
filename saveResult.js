const createCsvWriter = require('csv-writer').createObjectCsvWriter;

const saveResult = async (results) => {
  const csvWriter = createCsvWriter({
    path: './output.csv',
    append: true,
    header: [
      {id: 'slug', title: 'Slug'},
      {id: 'diff', title: 'Diff'}
    ]
});
  await csvWriter.writeRecords([results]);    // returns a promis
}

module.exports = saveResult;