const puppeteer = require('puppeteer').default;
const { BASELINE, CHANGED } = require('./constants');
const getPageText = require('./getPageText');
const produceDiff = require('./produceDiff');


const checkPage = async (pageSlug) => {
  console.log('>> checking', pageSlug);
  const browser = await puppeteer.launch();
  const baselineBodyText = await getPageText(browser, `${BASELINE}/${pageSlug}`);
  const changedBodyText = await getPageText(browser, `${CHANGED}/${pageSlug}`);

  await browser.close();

  return produceDiff(baselineBodyText, changedBodyText);
}

module.exports = checkPage;