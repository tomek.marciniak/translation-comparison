const fetch = require('node-fetch-commonjs');
const fs = require('fs');
const { SLUGS } = require('./constants');

const fetchPageSlugs = async () => {
  const slugData = await fetch(SLUGS).then(res => res.json());
  const mappedSlugs = [];

  slugData.forEach(item => {
    mappedSlugs.push(item.slug);
    if (item.localizations) {
      mappedSlugs.push(...item.localizations.map(locItem => locItem.slug));
    }
  });

  const sortedSlugs = mappedSlugs.sort();
  let lastIndex = 0;

  try {
    const data = fs.readFileSync('./output.csv', 'utf8');
    const dataArr = data.split('\n');
    dataArr.pop();
    const [lastSlug,] = dataArr.pop().split(',');
    lastIndex = sortedSlugs.indexOf(lastSlug) + 1;
  } catch (err) {

  }

  return sortedSlugs.slice(lastIndex);
}

module.exports = fetchPageSlugs;