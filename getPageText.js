

const getPageText = async (browser, url) => {
  const page = await browser.newPage();

  await page.goto(url);
  await page.waitForNavigation({ waitUntil: 'networkidle0' });
  await page.setViewport({width: 1300, height: 1024});

  const body = await page.waitForSelector('body');
  const bodyText = await body.evaluate(el => el.textContent);

  return bodyText;
}

module.exports = getPageText;